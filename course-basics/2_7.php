<?php
  class User {
    public $name;
    public $age;

    public static $minPassLenght = 6;

    public static function validatePass($pass) {
      if(strlen($pass) >= self::$minPassLenght) {
        return true;
      } else {
        return false;
      }
    }
  }

  $passsword = 'hello';

  if(User::validatePass($passsword)) {
    echo 'Password valid';
  } else {
    echo 'Password not valid';
  }