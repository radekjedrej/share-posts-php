<?php
  // Define class
  class User {
    // Properties (attribiutes)
    public $name = 'Radek';

    // Methods (functions)
    public function sayHello() {
      return $this->name . ' Hey There!!';
    }
  }

  // Instantiate a user object from the user class
  $user1 = new User();
  echo '<br>';
  echo $user1->sayHello();

  $user2 = new User();
  echo '<br>';
  $user2->name = 'Maciek';
  echo $user2->sayHello();
  
