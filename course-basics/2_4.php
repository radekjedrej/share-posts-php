<?php
  class User {
    public $name;
    public $age;

    // Runs when object is created
    public function __construct($name, $age) {
      // echo 'constructor run';
      echo 'Class ' . __CLASS__ . ' instantiated ';
      echo '<br>';
      $this->name = $name;
      $this->age = $age;
    }

    public function sayHello(){
      return $this->name . ' Says Hello!';
    }

    // Called when no other references to a certain object
    // Used for cleanup, closinng connection, etc.
    public function __destruct() {
      echo '<br>';
      echo 'destruct run';
    }
  }


  $user1 = new User('Brad', 36);
  echo $user1->name . ' is ' . $user1->age . ' years old';
  echo '<br>';
  echo $user1->sayHello();
  echo '<br>';

  $user2 = new User('Radek', 31);
  echo $user2->name . ' is ' . $user2->age . ' years old';
  echo '<br>';
  echo $user2->sayHello();